<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php confirm_logged_in(); ?>

<?php $layout_context = "admin"; ?>
<?php include("../includes/layouts/header.php"); ?>

<div id="main">
  <div id="navigation">
    &nbsp;
  </div>
  <div id="page">
    <h2>Admin Menu</h2>
    <p>witaj w panelu admina, <?php echo htmlentities($_SESSION["username"]); ?>.</p>
    <ul>
      <li><a href="manage_content.php">Zarzadzaj trescia</a></li>
      <li><a href="manage_admins.php">Zarzadzaj uzytkowikami</a></li>
      <li><a href="logout.php">Wyloguj</a></li>
    </ul>
  </div>
</div>

<?php include("../includes/layouts/footer.php"); ?>
